import React from "react";
import { Box } from "@mui/material";

export const ContainerBox = ({ children }) => {
  return (
    <Box
      sx={{
        maxWidth: "1200px",
        margin: "auto",
        padding: { xs: "0 10px", md: "0" },
      }}
    >
      {children}
    </Box>
  );
};
