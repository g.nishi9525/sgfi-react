import React from "react";
import CountUp from "react-countup";

export default function Counter({ number, title }) {
    console.log("check")
  return (
    <div className="number">
      <CountUp duration={6} className="count-digit" end={number} /><span className="count-digit">+</span>
      <div className="count-title">{title}</div>
    </div>
  );
}
