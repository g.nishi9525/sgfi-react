import { Button, Typography } from "@mui/material";
import React from "react";

export const StyledButton = ({ placeholder, width, classname = '' }) => {
  return (
    <Typography
      sx={{
        // fontFamily: "Manrope, sans-serif",
        fontSize: "14px",
        fontWeight: 600,
        textTransform: "uppercase",
        letterSpacing: "0.5px",
        backgroundColor: "#F6AD15",
        cursor: "pointer",
        transition: "all .2s linear",
        "&:hover": {
          backgroundColor: "#B9331D",
        },
      }}
      fill="#FFFFFF"
      color="#FFFFFF"
      borderRadius="35px"
      padding="15px 30px"
      width={width ? { xs: "100%", md: width } : "100%"}
      textAlign="center"
      className={classname}
    >
      {placeholder}
    </Typography>
  );
};
