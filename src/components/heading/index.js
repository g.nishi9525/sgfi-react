import { Box, Typography } from "@mui/material";
import React from "react";

export const SectionHeading = ({ smallHeading, largeHeading, textAlign }) => {
  return (
    <Box
      sx={{
        position: "relative",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: textAlign ? textAlign : { xs: "center", md: "start" },
      }}
    >
      <Typography
        sx={{
          textAlign: textAlign ? textAlign : { xs: "center", md: "start" },
          color: "#FD6047",
          fontSize: "18px",
          fontWeight: "600",
          textTransform: "uppercase",
        }}
      >
        {smallHeading}
      </Typography>
      <Typography
        sx={{
          textAlign: textAlign ? textAlign : { xs: "center", md: "start" },
          color: "#090809",
          fontSize: "2rem",
          fontWeight: 600,
          lineHeight: "1.3em",
          letterSpacing: "-1px",
          textTransform: "capitalize",
        }}
      >
        {largeHeading}
      </Typography>
      <Box
        sx={{
          marginTop: "10px",
          width: "30%",
          background: "#F6AD15",
          height: "3px",
          // position: "absolute",
          bottom: "-10px",
        }}
      ></Box>
    </Box>
  );
};
