import img1 from "assets/img1.jpg";
import img2 from "assets/img2.jpg";
import img3 from "assets/img3.jpg";
import img4 from "assets/img4.jpg";
import img5 from "assets/img5.jpg";
import img6 from "assets/img6.jpg";
import {
  Card,
  CardContent,
  Typography,
  Button,
  makeStyles,
} from "@mui/material";
import "./gallery.css";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useScreenResizer } from "utils/useScreenSize";
import { ContainerBox } from "components";
import { Zoom } from "react-awesome-reveal";

const Gallery = () => {
  const slides = [
    {
      image: img1,
      heading: "Slide 1",
      description: "This is the first slide",
    },
    {
      image: img2,
      heading: "Slide 2",
      description: "This is the second slide",
    },
    {
      image: img3,
      heading: "Slide 3",
      description: "This is the third slide",
    },
    {
      image: img4,
      heading: "Slide 2",
      description: "This is the second slide",
    },
    {
      image: img5,
      heading: "Slide 3",
      description: "This is the third slide",
    },
    {
      image: img6,
      heading: "Slide 3",
      description: "This is the third slide",
    },
  ];

  const CustomPrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <Button
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        Previous
      </Button>
    );
  };

  const CustomNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <Button
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        Next
      </Button>
    );
  };

  const screenSize = useScreenResizer(["sm", "md", "lg"]);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: screenSize === "lg" ? 3 : screenSize === "md" ? 2 : 1,
    slidesToScroll: 1,
    arrows: true,
  };

  console.log("screenSize", screenSize);
  return (
    <section>
      <ContainerBox>
        <Slider {...settings}>
          {slides.map((slide, index) => (
            <div className="gallery-cards" key={index}>
              <Zoom>
                <Card className={"gallery-card"}>
                  <img
                    loading="lazy"
                    src={slide.image}
                    className="gallery-image"
                    alt={slide.heading}
                  />
                  {/* <CardContent>
                <Typography variant="h5" component="h2">
                  {slide.heading}
                </Typography>
                <Typography variant="body2" component="p">
                  {slide.description}
                </Typography>
              </CardContent> */}
                </Card>
              </Zoom>
            </div>
          ))}
        </Slider>
      </ContainerBox>
    </section>
  );

  // return (
  //   <Carousel
  //     autoPlay={false} // Set to true if you want the carousel to auto-play
  //     animation="slide" // Animation type (slide/fade)
  //     navButtonsAlwaysVisible={true} // Show navigation buttons always
  //     next={() => console.log('Next')} // Function to handle next button click
  //     prev={() => console.log('Previous')} // Function to handle previous button click
  //     slidesToShow={3}
  //   >
  //     {slides.map((slide, index) => (
  //       <Card key={index} className={'card'}>
  //         <img src={slide.image} alt={slide.heading} />
  //         <CardContent>
  //           <Typography variant="h5" component="h2">
  //             {slide.heading}
  //           </Typography>
  //           <Typography variant="body2" component="p">
  //             {slide.description}
  //           </Typography>
  //         </CardContent>
  //       </Card>
  //     ))}
  //   </Carousel>

  // );
};

export default Gallery;
