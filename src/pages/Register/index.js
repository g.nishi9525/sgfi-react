import React from "react";
import { ContainerBox, SectionHeading, StyledButton } from "components";
import { Box, Typography } from "@mui/material";
import basketball from "assets/basketball.jpeg";
import cricket from "assets/crick.jpeg";
import "./register.css";
import { Fade } from "react-awesome-reveal";

export const Register = () => {
  return (
    <section className="register-container section" id="register">
      <ContainerBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: { xs: "column", md: "row" },
            justifyContent: "space-between",
            alignItems: "center",
            gap: "1rem",
          }}
        >
          <Box
            sx={{
              width: { xs: "100%", md: "40%" },
              display: "flex",
              justifyContent: "start",
              alignItems: { xs: "center", md: "start" },
              gap: "1.5rem",
              flexDirection: "column",
            }}
          >
            <Fade direction="left">
              <SectionHeading
                smallHeading="Register Today"
                largeHeading="Register today and becoma a part of SGFI."
              />
              <Typography textAlign={{ xs: "center", md: "start" }}>
                We are constantly working on providing better facilities and
                infrastructure to the particpants.
              </Typography>
              <StyledButton placeholder="Register Now" width="100%" />
            </Fade>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              gap: "20px",
              flexWrap: "wrap",
            }}
          >
            <Fade direction="right">
              <Box
                sx={{
                  borderRadius: { xs: "0", md: "20px" },
                  overflow: "hidden",
                  display: { xs: "flex", md: "inherit" },
                  alignItems: "center",
                  justifyContent: "center",
                  width: { xs: "100%", md: "300px" },
                  height: { xs: "100%", md: "300px" },
                  padding: { xs: "0 10px", md: "0" },
                }}
              >
                <img
                  loading="lazy"
                  src={basketball}
                  alt=""
                  style={{ width: "100%", height: "100%" }}
                />
                <Box sx={{ background: "#F6AD15", height: "100px" }}></Box>
              </Box>
              <Box
                sx={{
                  borderRadius: "20px",
                  overflow: "hidden",
                  display: { xs: "none", md: "block" },
                  width: { xs: "100%", md: "350px" },
                  height: "400px",
                }}
              >
                <img
                  src={cricket}
                  alt=""
                  style={{
                    objectFit: "cover ",
                    width: "100%",
                    height: "100%",
                  }}
                  loading="lazy"
                />
              </Box>
            </Fade>
          </Box>
        </Box>
      </ContainerBox>
    </section>
  );
};
