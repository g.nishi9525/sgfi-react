import { Fade } from "react-awesome-reveal";
import "./contactUs.css";
import { ContainerBox, SectionHeading } from "components";

export const ContactUs = () => {
  return (
    <section class="contact-us" id="contact">
      <ContainerBox>
        <div class="contact-container">
          <div class="contact-form">
            <Fade direction="left">
              <form id="contact" action="" method="post">
                <div class="row">
                  <div class="col-lg-12">
                    <h2>Let's get in touch</h2>
                  </div>
                  <div class="col-lg-4">
                    <fieldset>
                      <input
                        name="name"
                        type="text"
                        id="name"
                        placeholder="YOURNAME...*"
                        required=""
                      />
                    </fieldset>
                  </div>
                  <div class="col-lg-4">
                    <fieldset>
                      <input
                        name="email"
                        type="text"
                        id="email"
                        pattern="[^ @]*@[^ @]*"
                        placeholder="YOUR EMAIL..."
                        required=""
                      />
                    </fieldset>
                  </div>
                  <div class="col-lg-4">
                    <fieldset>
                      <input
                        name="subject"
                        type="text"
                        id="subject"
                        placeholder="SUBJECT...*"
                        required=""
                      />
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <textarea
                        name="message"
                        type="text"
                        class="form-control"
                        id="message"
                        placeholder="YOUR MESSAGE..."
                        required=""
                      ></textarea>
                    </fieldset>
                  </div>
                  <div class="col-lg-12">
                    <fieldset>
                      <button type="submit" id="form-submit" class="button">
                        SEND MESSAGE NOW
                      </button>
                    </fieldset>
                  </div>
                </div>
              </form>
            </Fade>
          </div>
          <div class="right-info">
            <Fade direction="right">
              <ul>
                {/* <li>
                <h6>
                  The purpose of Student game federation is to develop the
                  children mentally, socially and physically from school level.
                  To organize all competitions under one flag and increase
                  communal harmony in country.
                </h6>
              </li> */}
                <li>
                  <h6>Phone Number</h6>
                  <span>+ 91-88902 72222</span>
                </li>
                <li>
                  <h6>Email Address</h6>
                  <span>info@meeting.edu</span>
                </li>
                <li>
                  <h6>Street Address</h6>
                  <span>
                    Student Games Federation of India Address :- Roru bari
                  </span>
                  <span>Laxmangarh, Sikar (Rajasthan) India 332317</span>
                </li>
                <li>
                  <h6>Website URL</h6>
                  <span>www.meeting.edu</span>
                </li>
              </ul>
            </Fade>
          </div>
        </div>
      </ContainerBox>
    </section>
  );
};
