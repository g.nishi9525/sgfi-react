import { ContainerBox, StyledButton } from "components";
import React from "react";
import "./banner.css";
import Neon from "assets/Neon.mp4";
import { Box, Typography } from "@mui/material";
import { Fade, Slide, Zoom } from "react-awesome-reveal";

export const Banner = () => {
  return (
    <Box className="banner" id="banner">
      <Box sx={{ position: "relative", height: "100vh" }}>
        <video className="background-video" autoPlay loop muted>
          <source src={Neon} type="video/mp4" />
        </video>
      </Box>
      <>
        <Box className="overlay" sx={{ position: "absolute" }}>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100%",
            }}
          >
            <Zoom>
              <Typography sx={{ color: "#fff" }}>
                <Box class="typewriter">
                  <h1>WELCOME TO SGFI</h1>
                  <Typography sx={{ fontSize: "1.4rem", padding: "1rem 0" }}>
                    Best Federation in India
                  </Typography>
                  <Box sx={{ paddingTop: "1rem" }}>
                    <StyledButton placeholder="About Us" />
                  </Box>
                </Box>
              </Typography>
            </Zoom>
          </Box>
        </Box>
      </>
    </Box>
  );
};
