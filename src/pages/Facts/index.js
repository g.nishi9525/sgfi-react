import "./facts.css";
import gamesImage from "assets/games.png";
import wrestlingImage from "assets/wrestling.png";
import { Grid, ListItem, Paper } from "@mui/material";
import Counter from "components/counter";
import { ContainerBox } from "components";
import { Fade, Slide, Zoom } from "react-awesome-reveal";

export const Facts = () => {
  const gamesData = [
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },

    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
    {
      name: "Wrestling",
      href: "https://www.youtube.com/watch?v=HndV87XpkWg",
      src: wrestlingImage,
    },
  ];

  return (
    <section class="our-facts section" id="facts">
      <ContainerBox>
        <h2>A Few Facts About Our Organisation</h2>
        <div class="facts-info-container">
          <div class="facts-info">
            <div class="count-area-content">
              <Zoom>
                <Counter number={4000} title="Positive Reviews" />
              </Zoom>
            </div>
          </div>
          <div class="facts-info">
            <div class="count-area-content">
              <Zoom>
                <Counter number={230} title="Events" />
              </Zoom>
            </div>
          </div>
          <div class="facts-info">
            <div class="count-area-content ">
              <Zoom>
                <Counter number={250} title="Days" />
              </Zoom>
            </div>
          </div>

          <div class="facts-info">
            <div class="count-area-content">
              <Zoom>
                <Counter number={3250} title="Happy Winner" />
              </Zoom>
            </div>
          </div>
        </div>
        <div class="align-self-center">
          <div class="item">
            <img
              src={gamesImage}
              class="games-img"
              alt="Course One"
              loading="lazy"
            />
          </div>
        </div>

        <div class="white-background">
          <div class="games-heading">Games</div>
          <Grid container className="grid-container" spacing={2}>
            {gamesData.map((item, index) => (
              <Grid
                item
                lg={2}
                xs={6}
                sm={4}
                className="grid-item"
                key={1}
                index={index}
              >
                <Zoom>
                  <div class="item">
                    <a href={item.href} target="_blank" rel="noreferrer">
                      <div class="games-image-icon">
                        <img src={item.src} alt="" loading="lazy" />
                      </div>
                      <span>{item.name}</span>
                    </a>
                  </div>
                </Zoom>
              </Grid>
            ))}
          </Grid>
        </div>
      </ContainerBox>
    </section>
  );
};
