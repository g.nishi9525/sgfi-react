import React from "react";
import "./quote.css";
import { Box } from "@mui/material";
import { ContainerBox } from "components";

export const Quote = () => {
  return (
    <section className="quote section">
      <Box>
        {/* <Box
            class="quote-wrap quote-style-3"
            sx={{ width: { xs: "auto", md: "500px" } }}
          >
            <Box class="quote-thumb"></Box>
            <blockquote>
              <p>WE BELIEVE IN GIVING BACK TO THE SOCIETY</p>
            </blockquote>
            <div class="quote-attribution">
              <p class="quote-author">JITENDRA</p>
              <cite>Secretary</cite>
            </div>
          </Box> */}

        <Box className="menu-container" sx={{ padding: "3rem" }}>
          <ContainerBox>
            <blockquote>We believe in giving back to the society.</blockquote>
            <cite>- JITENDRA, SECRETARY</cite>
          </ContainerBox>
        </Box>
      </Box>
    </section>
  );
};
