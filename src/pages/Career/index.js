import "./career.css";
import background from "assets/background.mp4";
import { Box } from "@mui/material";
import { StyledButton } from "components";

export const Career = () => {
  return (
    <section className="career-section">
      <Box sx={{ position: "relative", height:"70vh" }}>
        <video className="background-video" autoPlay loop muted>
          <source src={background} type="video/mp4" />
        </video>
      </Box>
      <Box className="career-info">
        <p>BUILD YOUR SPORTS CAREER TODAY WITH SGFI</p>
        <div className="career-btn">
          <StyledButton
            placeholder="Register Now"
            classname={"career-register-btn"}
          />
        </div>
      </Box>
    </section>
  );
};
