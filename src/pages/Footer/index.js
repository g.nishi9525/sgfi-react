import "./footer.css";
import FacebookOutlinedIcon from "@mui/icons-material/FacebookOutlined";
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import YouTubeIcon from "@mui/icons-material/YouTube";
import logo from "assets/main.logo.png";
import { ContainerBox } from "components";
import { Fade, Slide, Zoom } from "react-awesome-reveal";
import { Link } from "react-router-dom";

export const Footer = () => {
  return (
    <section class="footer-section">
      <div className="footer-container">
        <ContainerBox>
          <div className="footer-info">
            <div className="footer-about-us footer-item">
              <Fade direction="left">
                <h3>About Us</h3>
                <p>
                  Pariatur eiusmod exercitationem? Maxime, repellendus repellat,
                  eu vitae! Sociosqu etiam cursus curabitur. Eveniet dictumst
                  cura. Nesciunt quidam comod ab
                </p>
              </Fade>
            </div>
            <div className="footer-foundation footer-item">
              <h3>Our Foundation</h3>
              <Fade direction="left">
                <ul className="footer-list">
                  <li>
                    <Link to="">About us</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Testimonials</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Our Volunteers</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Recent Causes</Link>
                  </li>
                  <hr />
                </ul>
              </Fade>
            </div>
            <Fade direction="left">
              <div className="footer-follow footer-item">
                <h3>Follow Us On</h3>
                <p>
                  <p>
                    Address : 2976 Washington St San Francisco, CA 94115 United
                    States
                  </p>
                  <br />
                  <p>New york : + 1-677-124-44227</p>
                  <p>London : + 1-677-124-44227</p>
                  <p>Mail : yourname@gmail.com</p>
                </p>
              </div>
            </Fade>
            <div className="footer-links footer-item">
              <h3>Useful Links</h3>
              <Fade direction="left">
                <ul className="footer-list">
                  <li>
                    <Link to="">Help Center</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Contact Us</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Donation Method</Link>
                  </li>
                  <hr />
                  <li>
                    <Link to="">Parent Community</Link>
                  </li>
                  <hr />
                </ul>
              </Fade>
            </div>
          </div>

          <hr />
          <div className="footer-social-media">
            <Slide>
              <div className="media-icons">
                <FacebookOutlinedIcon
                  fontSize="large"
                  className={"media-icon"}
                />
                <InstagramIcon fontSize="large" className={"media-icon"} />
                <LinkedInIcon fontSize="large" className={"media-icon"} />
                <YouTubeIcon fontSize="large" className={"media-icon"} />
              </div>
            </Slide>

            <div className="text-center">
              <Fade direction="up">
                <ul className="footer-list privacy-terms">
                  <li>
                    <Link to="">Privacy Policy</Link> |{" "}
                  </li>
                  <li>
                    <Link to="">Terms & Condition</Link> |{" "}
                  </li>
                  <li>
                    <Link to="">Faq</Link>
                  </li>
                </ul>
              </Fade>
              <p>Copyright © 2023 Student Games Federation of India.</p>
            </div>
            <div className="">
              <Zoom>
                <img
                  loading="lazy"
                  src={logo}
                  alt="main-logo"
                  style={{ width: "80px", height: "80px" }}
                />
              </Zoom>
            </div>
          </div>
        </ContainerBox>
      </div>
    </section>
  );
};
