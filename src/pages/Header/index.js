import { Box, Typography } from "@mui/material";
import { ContainerBox } from "components";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { navBarLinks } from "utils/constant";
import logo from "assets/main.logo.png";
import "./header.css";
import { Fade, Zoom } from "react-awesome-reveal";

export const Header = () => {
  const [showMenu, setShowMenu] = useState(false);

  const toggleMenu = () => {
    setShowMenu(!showMenu);
  };

  const [current, setCurrent] = useState("");

  useEffect(() => {
    const handleScroll = () => {
      const sections = document.querySelectorAll(".section");
      const navLi = document.querySelectorAll("nav-bar-li");

      sections.forEach((section) => {
        const sectionTop = section.offsetTop;
        if (window.scrollY >= sectionTop) {
          setCurrent(section.getAttribute("id"));
        }
      });

      navLi.forEach((li) => {
        li.classList.remove("active");
        console.log("================================", li.classList.contains);
        if (li.classList.contains(current)) {
          li.classList.add("active");
        }
      });
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [current]);

  return (
    <>
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          background: "#E1E1E1",
          display: { xs: "none", md: "block" },
          zIndex: "100",
          opacity: ".7",
        }}
      >
        <ContainerBox>
          <Box
            sx={{
              padding: "5px 0",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Fade direction="left">
              <Box sx={{}}>
                <img
                  loading="lazy"
                  src={logo}
                  alt="main-logo"
                  style={{ width: "80px", height: "80px" }}
                />
              </Box>
            </Fade>
            <Box>
              <ul
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  gap: "15px",
                }}
              >
                {navBarLinks?.map((item, index) => {
                  return (
                    <NavLink
                      style={({ isActive }) => ({
                        borderBottom: isActive ? "2px solid" : "inherit",
                        textTransform: "capitalize",
                      })}
                      to={item?.url}
                      key={index}
                      className="nav-bar-li "
                    >
                      <Fade direction="down">
                        <Typography
                          className={item?.class}
                          sx={{ textTransform: "capitalize" }}
                        >
                          {item?.linkName}
                        </Typography>
                      </Fade>
                    </NavLink>
                  );
                })}
              </ul>
            </Box>
          </Box>
        </ContainerBox>
      </Box>
      <Box sx={{ display: { xs: "flex", md: "none" }, width: "100%" }}>
        <div className="brand">
          <img loading="lazy" src={logo} alt="" />
        </div>
        <div
          className={`menu-btn ${showMenu ? "close" : ""}`}
          onClick={toggleMenu}
        >
          <div className="btn-line"></div>
          <div className="btn-line"></div>
          <div className="btn-line"></div>
        </div>

        {/* <nav className={`menu ${showMenu ? "show" : ""}`}>
          <ul className="menu-nav">
            <li className="nav-item current">
              <a href="#home" className="nav-link">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a href="about.html" className="nav-link">
                About
              </a>
            </li>
            <li className="nav-item">
              <a href="work.html" className="nav-link">
                Work
              </a>
            </li>
            <li className="nav-item">
              <a href="contact.html" className="nav-link">
                Contact
              </a>
            </li>
          </ul>
        </nav> */}
      </Box>
    </>
  );
};
