import React from "react";
import "./about.css";
import { ContainerBox, SectionHeading, StyledButton } from "components";
import { Box, Typography } from "@mui/material";
import pic from "assets/playersgroup.jpeg";
import { Fade } from "react-awesome-reveal";

export const About = () => {
  return (
    <section className="about section">
      <ContainerBox>
        <Box
          sx={{
            display: "flex",
            flexDirection: { xs: "column", md: "column" },
            justifyContent: "space-between",
            alignItems: "center",
            gap: "1rem",
          }}
        >
          <Fade direction="down">
            <Box
              sx={{
                width: { xs: "100%" },
                display: "flex",
                justifyContent: "start",
                alignItems: "center",
                gap: "1.5rem",
                flexDirection: "column",
              }}
            >
              <SectionHeading
                textAlign="center"
                smallHeading="About us"
                largeHeading="develop the children mentally, socially and physically"
              />
              <Typography textAlign={{ xs: "center" }}>
                The purpose of Student game federation is to develop the
                children mentally, socially and physically from school level. To
                organize all competitions under one flag and increase communal
                harmony in country. There are different games of national level,
                district level and state level of different education board like
                CBSE, ICSE, ISE, CVE.
              </Typography>
              <StyledButton placeholder="Read More" width="30%" />
            </Box>
          </Fade>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              gap: "20px",
              flexWrap: { xs: "wrap", md: "inherit" },
            }}
          >
            <Fade direction="up">
              <Box sx={{ borderRadius: "20px", overflow: "hidden" }}>
                <img
                  src={pic}
                  alt=""
                  loading="lazy"
                  style={{ width: "100%", height: "auto" }}
                />
              </Box>
            </Fade>
          </Box>
        </Box>
      </ContainerBox>
    </section>
  );
};
