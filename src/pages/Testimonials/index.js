import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import { ContainerBox, SectionHeading } from "components";
import React from "react";
import founderImage from "assets/founder.gulabsingh.jpg";
import "./testimonials.css";
import { Box } from "@mui/system";
import FormatQuoteOutlinedIcon from "@mui/icons-material/FormatQuoteOutlined";
import { Icon } from "@mui/material";
import { Fade, Slide, Zoom } from "react-awesome-reveal";
export const Testimonials = () => {
  const testimonialsData = [
    {
      name: "Gulab Singh",
      image: founderImage,
      designation: "Founder & Gen. secretary",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus,luctus nec ullamcorper mattis, pulvinar adipiscing dapibus luctus leo.",
    },
    {
      name: "Jitender Subfg",
      image: founderImage,
      designation: "Working secretary",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus,luctus nec ullamcorper mattis, pulvinar adipiscing dapibus luctus leo.",
    },
    {
      name: "Sunil Kumar",
      image: founderImage,
      designation: "President",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus,luctus nec ullamcorper mattis, pulvinar adipiscing dapibus luctus leo.",
    },
  ];

  return (
    <section className="testimonials-section section" id="testimonials">
      <ContainerBox>
        <SectionHeading
          textAlign="center"
          smallHeading="Testimonials"
          largeHeading="Our Supporters Voice"
          maxWidth="20%"
        />
        <p className="description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit
          tellus, luctus nec ullamcorper mattis, pulvinar adipiscing dapibus
          luctus leo.
        </p>

        <Box className="cards">
          {testimonialsData.map((cardItem) => (
            <Card
              sx={{
                maxWidth: 500,
                borderRadius: "10px",
                boxShadow: "0px 0px 10px 0px #e1e1e1 ",
              }}
              className="card"
            >
              <Slide>
                <CardContent
                  className="card-content"
                  sx={{
                    display: "flex",
                    flexDirection: { xs: "column", md: "row" },
                  }}
                >
                  {/* <CardMedia
                sx={{ height: "100px", width: "100px", borderRadius: "50%" }}
                image={cardItem.image}
                title="green iguana"
              /> */}
                  <Zoom>
                    <img
                      loading="lazy"
                      src={cardItem.image}
                      className="image"
                      alt=""
                    />
                  </Zoom>
                  <Box className="content">
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div"
                      textAlign="center"
                    >
                      {cardItem.name}
                    </Typography>
                    <Typography
                      variant="body2"
                      className="red"
                      textAlign="center"
                    >
                      {cardItem.designation}
                    </Typography>
                  </Box>
                  <FormatQuoteOutlinedIcon color="red" fontSize="large" />
                  <i aria-hidden="true" class="icon icon-quote"></i>
                </CardContent>
                <Typography
                  variant="body2"
                  color="text.secondary"
                  textAlign="center"
                >
                  {cardItem.info}
                </Typography>
              </Slide>
            </Card>
          ))}
        </Box>
      </ContainerBox>
    </section>
  );
};
