import HomeIcon from "@mui/icons-material/Home";
import PersonIcon from "@mui/icons-material/Person";
import CodeIcon from "@mui/icons-material/Code";
import WorkIcon from "@mui/icons-material/Work";
import BookIcon from "@mui/icons-material/Book";
import PermContactCalendarIcon from "@mui/icons-material/PermContactCalendar";

export const navBarLinks = [
  {
    linkName: "Home",
    linkImage: <HomeIcon />,
    url: "/",
    class: "home",
  },
  {
    linkName: "About Us",
    linkImage: <PersonIcon />,
    url: "/about-us",
    class: "about",
  },
  {
    linkName: "Reviews",
    linkImage: <CodeIcon />,
    url: "/reviews",
    class: "facts",
  },
  {
    linkName: "Registrations",
    linkImage: <WorkIcon />,
    url: "/Registrations",
    class: "register",
  },
  {
    linkName: "Affiliated Units",
    linkImage: <BookIcon />,
    url: "affiliated-units",
    class: "",
  },
  {
    linkName: "Team",
    linkImage: <PermContactCalendarIcon />,
    url: "/team",
    class: "team",
  },
  {
    linkName: "Student Premier League",
    linkImage: <PermContactCalendarIcon />,
    url: "/student-premier-league",
    class: "student-premier-league",
  },
  {
    linkName: "Player Records",
    linkImage: <PermContactCalendarIcon />,
    url: "/player-records",
    class: "player-records",
  },
];
