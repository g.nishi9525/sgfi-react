import { useState, useEffect } from 'react';
import { useMediaQuery } from './useMediaQuery';

/**
 * The parameter must contain an array of 3 values, the sequence in which these values should be:
 * [sm_related_value, md_related_value, lg_related_value]
 */
export const useScreenResizer = (values, defaultIndex = 0) => {
  const [value, setValue] = useState(values[defaultIndex]);
  const isMobile = useMediaQuery('(max-width: 480px)');
  const isDesktop = useMediaQuery('(min-width: 961px)');

  useEffect(() => {
    if (isMobile) {
      setValue(values[0]);
    } else if (isDesktop) {
      setValue(values[2]);
    } else {
      setValue(values[1]);
    }
  }, [values, isMobile, isDesktop]);

  return value;
};
