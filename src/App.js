import { Box } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
  About,
  Banner,
  Facts,
  Header,
  Quote,
  Register,
  Testimonials,
  ContactUs,
  Footer,
  Career,
} from "pages";
import Gallery from "pages/Gallery";
import { Route, Routes } from "react-router-dom";
import "./globalStyles.css";

export function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Poppins", "Manrope"].join(","),
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <Box>
        <Header />
        <Banner />
        <Register />
        {/* <Quote /> */}
        <About />
        <Facts />
        <Gallery />
        <Career />
        <Testimonials />
        <ContactUs />
        <Footer />
      </Box>
    </ThemeProvider>
  );
}
